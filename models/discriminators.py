import tensorflow as tf

layers = tf.contrib.layers


def discriminator3D(img, unused_conditioning, weight_decay=0):

    with tf.contrib.framework.arg_scope([layers.fully_connected, layers.conv3d],
                                         activation_fn=tf.nn.relu):

        layer = layers.conv3d(img, 32, [4,4,4], stride=2)
        layer = layers.conv3d(layer, 64, [4,4,4], stride=2)
        layer = layers.conv3d(layer, 128, [4,4,4], stride=2)
        layer = layers.flatten(layer)

        return layers.fully_connected(layer, 1)