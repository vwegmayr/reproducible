import tensorflow as tf

layers = tf.contrib.layers


def generator3D(noise, weight_decay=0, is_training=True):

    noise = layers.flatten(noise)

    with tf.contrib.framework.arg_scope([layers.fully_connected, layers.conv3d_transpose],
                                         activation_fn=tf.nn.relu):

        layer = layers.fully_connected(noise, 1024)
        layer = layers.fully_connected(layer, 8*8*8*128)

        layer = tf.reshape(layer, [-1, 8, 8, 8, 128])

        layer = layers.conv3d_transpose(layer, 64, [4,4,4], stride=2)
        layer = layers.conv3d_transpose(layer, 32, [4,4,4], stride=2)
        layer = layers.conv3d_transpose(layer, 16, [4,4,4], stride=2)
        layer = layers.conv3d_transpose(layer, 8, [4,4,4], stride=2)

        layer = layers.conv3d(layer, 1, [4,4,4], stride=1)

        return layer