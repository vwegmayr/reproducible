import tensorflow as tf

from abc import ABC, abstractmethod
from handlers import features_from_handlers


class Parser(ABC):
    """Baseclass for feature parser

    Parsers are used during input feeding. They map deserialized
    tensors to parsed tensors.

    Attributes:
        parent (Parser): Instance of Parser,
            which is executed before this parser.
    """

    def __init__(self, parent):
        super(Parser, self).__init__()
        if parent is not None:
            assert isinstance(parent, Parser)
        self.parent = parent

    def parse(self, key, feature_dict):
        """Parse key tensor using tensors from feature_dict  

        Performs parent parsing first, if available.

        Args:
            key (str): Name of feature to be parsed
            feature_dict (dict): Dict of keys to
                python features, used to parse the requested
                key. Note: Parser might process more than one key
                to obtain the requested key.

        Returns:
            dict: {key: tf_feature}
        """
        if self.parent is not None:
            key, feature_dict = self.parent._parse(key, feature_dict)
        return self._parse(key, feature_dict)

    @abstractmethod
    def _parse(self, key, feature_dict):
        """Parser-specific mapping, without parents"""
        pass


class ReshapeParser(Parser):
    """Reshape a tensor

    Attributes:
        shape (list of ints): Desired shape of tensor
    """
    def __init__(self, shape=None, parent=None):
        super(ReshapeParser, self).__init__(parent)
        self.shape = shape

    def _parse(self, key, feature_dict):

        feature_dict[key] = tf.reshape(feature_dict[key], self.shape)

        return key, feature_dict


class IdentityParser(Parser):
    """Parse tensor as returned by de-serialization

    Simply return the tensor specified by key.
    """
    def __init__(self, parent=None):
        super(IdentityParser, self).__init__(parent)

    def _parse(self, key, feature_dict):
        return key, feature_dict


class DecodeRawParser(Parser):
    """Decode bytes tensor

    Decode a bytes tensor to a numeric type tensor.

    Attributes: 
        out_type (str): Desired numeric type,
            should match the type used during serialization.
    """
    def __init__(self, out_type="float32", parent=None):
        super(DecodeRawParser, self).__init__(parent)
        self.out_type = getattr(tf, out_type)

    def _parse(self, key, feature_dict):
        feature_dict[key] = tf.decode_raw(feature_dict[key], out_type=self.out_type)
        return key, feature_dict
        

def parse_features(feature_dict, keys_to_parsers):
    """Runs parsers for all keys

    Args:
        feature_dict: Mapping of keys to de-serialized tensors.
        keys_to_parsers (dict): Mapping of keys to parsers.

    Returns:
        dict: Mapping of keys to parsed tensors.
    """
    for key in list(feature_dict.keys()):
        _, feature_dict = keys_to_parsers[key].parse(key, feature_dict)

    return feature_dict


def parse_serial_batch(serial_batch, keys_to_parsers, keys_to_handlers):
    """Perform batch-wise deserialization and parsing.

    Args:
        serial_batch: One element of a dataset after batching
        keys_to_parsers (dict): Mapping of keys to parsers
        keys_to_handlers (dict): Mapping of keys to handlers, used to
            determine isfixedlength, shape and dtype of features.

    Returns:
        dict: Mapping of keys to parsed tensors for a single batch
    """
    keys_to_features = features_from_handlers(keys_to_handlers)

    features = tf.parse_example(serial_batch, features=keys_to_features)

    return parse_features(features, keys_to_parsers)