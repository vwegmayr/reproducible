# sacred-tf

The sacred-tf package combines [Sacred](https://sacred.readthedocs.io/en/latest/) for experiment tracking with the [Tensorflow Data](https://www.tensorflow.org/guide/datasets) input pipelines. While the first aims to improve reproducibility and organisation of experiments, the latter aims to make data feeding faster.


## Getting Started

We recommend to setup Miniconda to create a python environment.

```
conda env create -f environment sacred-tf
source activate sacred-tf
```

From the root folder you can run experiments as modules:

```
python -m exps.uncond_brain_gan.train
```

To run one test, or all the tests:

```
python -m unittest datasets.brain.test_serialize
...
python -m unittest

```

## Structure

### [datasets](datasets/)

The folder [datasets](datasets/) contains the ingredients for different data input pipelines. As an example, consider the common [mnist](datasets/mnist) dataset.
To use its data in your experiment:

```python
from sacred import Experiment
from datasets.mnist.input_fn import mnist_input_fn, mnist_input_fn_ingred

ex = Experiment("My Experiment", ingredients=[mnist_input_fn_ingred])

@ex.automain
def train()
    ...
    estimator.train(mnist_input_fn())
```

You can find an other example using the brain dataset to train a GAN [here](exps/uncond_brain_gan/train.py).

Each dataset is supposed to specify the following steps of the input pipeline:

1. **get_raw.py** copy or download the most original data source.
2. preprocess.py perform heavy preprocessing, which is too expensive on the fly. (TODO)
3. **serialize.py** convert original data (e.g. jpg images) to tensorflow record format.
4. **input_fn.py** feed data from tensorflow records to your experiment.

### [Sacredboard](https://github.com/chovanecm/sacredboard)

Having [set up mongodb](https://gitlab.vis.ethz.ch/vwegmayr/sacred-tf/tree/master#MongoDB), you can track your experiments in the database:

```
python <train.py with params> -m sacred
```

This will create a new entry in the sacred mongodb. You can view it like so:

```
sacredboard -m sacred --port 8001 --no-browser
```

If you are running the above command from a remote server, you need to make sure you have set up the port forward, so you can view the experiments in your browser.

### [MongoDB](https://docs.mongodb.com/manual/tutorial/install-mongodb-on-ubuntu/)

Start by downloading the binaries for Ubuntu 16.04:

```
wget https://fastdl.mongodb.org/linux/mongodb-linux-x86_64-ubuntu1604-4.0.3.tgz
mkdir mongodb
tar -xzf mongodb-linux-x86_64-ubuntu1604-4.0.3.tgz -C mongodb --strip-components=1
```

Add to your `.bashrc`:

```export PATH="$HOME/mongodb/bin:$PATH```

Start the mongodb daemon:

```
mkdir sacred-db
mongod --dbpath sacred-db --fork --logpath sacred-db
```

## Examples

### MNIST dataset

#### Get raw data

To download the raw MNIST dataset with the default parameters, simply run 

```
python -m datasets.mnist.get_raw -m sacred
```

In this case, the process is very simple: we use keras to download the MNIST data to `~/.keras/datasets/mnist.npz`.

```python
# datasets/mnist/get_raw.py
...
raw_dir = os.path.join(str(Path.home()), ".keras/datasets")
npz_file = os.path.join(raw_dir, "mnist.npz")
...
@mnist_raw_ingred.capture
def get_raw():
	tf.keras.datasets.mnist.load_data()
...
```

This "experiment" was also saved in the database, so we can later always look up how this raw dataset was obtained:

![](demos/mnist_get_raw.png)

#### Serialize data

The MNIST data is provided as .npz file, but to feed it to a tensorflow model, we convert it to the .tfrecord format.

While there are other options, this is the recommended way for all but small-scale experiments.

```
python -m datasets.mnist.serialize -m sacred
```

The command above creates the records `mnist_train_0.tfrecord` and `mnist_test_0.tfrecord` in the folder `datasets/mnist/records`.

Again, the process was tracked with sacred:

![](demos/mnist_serialize.png)