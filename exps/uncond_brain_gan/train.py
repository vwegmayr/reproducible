import datetime
import tensorflow as tf

from sacred import Experiment
from sacred.stflow import LogFileWriter

from datasets.brain.input_fn import brain_input_fn_ingred, brain_input_fn
from input_fn import shuffle_repeat_prefetch
from models.generators import generator3D
from models.discriminators import discriminator3D

tfgan = tf.contrib.gan


ex = Experiment("brain_gan_example", ingredients=[brain_input_fn_ingred])

@ex.config
def config():
    noise_dims = 128
    model_dir = datetime.datetime.now().strftime("%Y%m%d%H%M%S")

    save_summary_steps=1000
    save_checkpoints_steps=1000
    log_step_count_steps=1

@ex.capture
def extended_input_fn(noise_dims, brain_feeder):

    def add_noise_and_swap(feature_dict, label_dict):
        noise = tf.random_normal(
            [brain_feeder["batch_size"], noise_dims])
        return (noise, feature_dict["image"])

    def fn():

        dataset = brain_input_fn()()

        dataset = dataset.map(add_noise_and_swap,
            num_parallel_calls=brain_feeder["num_parallel"])

        return dataset

    return fn


@ex.automain
@LogFileWriter(ex)
def main(model_dir,
    save_summary_steps,
    save_checkpoints_steps,
    log_step_count_steps):

    config = tf.estimator.RunConfig(model_dir=model_dir,
                                    save_summary_steps=save_summary_steps,
                                    save_checkpoints_steps=save_checkpoints_steps,
                                    log_step_count_steps=log_step_count_steps)

    # Create GAN estimator.
    gan_estimator = tfgan.estimator.GANEstimator(
        model_dir,
        generator_fn=generator3D,
        discriminator_fn=discriminator3D,
        generator_loss_fn=tfgan.losses.wasserstein_generator_loss,
        discriminator_loss_fn=tfgan.losses.wasserstein_discriminator_loss,
        generator_optimizer=tf.train.AdamOptimizer(0.1, 0.5),
        discriminator_optimizer=tf.train.AdamOptimizer(0.1, 0.5),
        config=config)
    # Train estimator.
    gan_estimator.train(extended_input_fn())