import os

import tensorflow as tf

from ingredient_wrapper import Ingredient
from .serialize import brain_serializer_ingred, serialize
from parsers import ReshapeParser, IdentityParser, DecodeRawParser
from input_fn import input_fn_ingred, input_fn, any_record_exists


brain_input_fn_ingred = Ingredient("brain_feeder",
                                   ingredients=[input_fn_ingred,
                                                brain_serializer_ingred])

@brain_input_fn_ingred.config
def config(img_shape, img_dtype):

    keys_to_parsers = {
        "image": ReshapeParser([-1] + img_shape + [1],
                               parent=DecodeRawParser(img_dtype)),
        "image/shape": IdentityParser(),
        "age": IdentityParser()
    }

    feature_keys = ["image"]
    label_keys = ["age"]


@brain_input_fn_ingred.capture
def brain_input_fn():
    if not any_record_exists():
        serialize()
    
    return input_fn()