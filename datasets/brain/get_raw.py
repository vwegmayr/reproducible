import os

from ingredient_wrapper import Ingredient
from datasets.raw_data import raw_metacsv_ingred

raw_brain_ingred= Ingredient("brain_raw", ingredients=[raw_metacsv_ingred])

@raw_brain_ingred.config
def raw_data_updates():
	raw_dir = "datasets/brain/test_data"

@raw_brain_ingred.config
def raw_metacsv_updates(raw_dir):
	csv_file = "datasets/brain/test_data/meta_data.csv"

@raw_brain_ingred.capture
def get_raw(raw_dir):
	assert os.path.exists(raw_dir)
	assert os.path.exists(csv_file)

if __name__ == '__main__':

    ex = Experiment("Get_Raw_Brain", ingredients=[raw_brain_ingred])

    @ex.main
    def main():
    	get_raw()

    ex.run_commandline()

