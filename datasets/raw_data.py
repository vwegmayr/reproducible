from ingredient_wrapper import Ingredient

raw_data_ingred = Ingredient("raw_data")
"""Raw Data Ingredient

Raw data refers to data stored in some file or database format.
"""
@raw_data_ingred.config
def config():
	raw_dir = "" # Directory where raw data is saved
	is_splitted = False # Whether raw data is already splitted into train/eval/test


raw_npz_ingred = Ingredient("raw_npz", ingredients=[raw_data_ingred])
"""Raw NPZ Ingredient

Raw npz data refers to files stored in the numpy format npz.
"""
@raw_npz_ingred.config
def config():
	xy_to_key = {} # Specifies which keys in npz refer to X and y 


raw_metacsv_ingred = Ingredient("raw_metacsv", ingredients=[raw_data_ingred])
"""Raw Meta-CSV Ingredient

The meta-csv contains only the meta-information about the raw data.
For example, the raw data might be a folder of medical images, and
the meta-csv contains info like age, gender, subject id, image id, etc.
"""
@raw_metacsv_ingred.config
def config(is_splitted):
	csv_file = "" # Path to meta-csv
	split_column = None # Name of column indicating which split a sample belongs to
	if is_splitted:
		assert split_column is not None