import os
import tensorflow as tf

from pathlib import Path
from sacred import Experiment
from ingredient_wrapper import Ingredient
from datasets.raw_data import raw_npz_ingred

mnist_raw_ingred= Ingredient("mnist_raw", ingredients=[raw_npz_ingred])


@mnist_raw_ingred.config
def raw_data_updates():
	raw_dir = os.path.join(str(Path.home()), ".keras/datasets")
	is_splitted = True

@mnist_raw_ingred.config
def raw_npz_updates():
	xy_to_key = {
		"x": "x_{split}",
		"y": "y_{split}",
	}

@mnist_raw_ingred.config
def config(raw_dir):
	npz_file = os.path.join(raw_dir, "mnist.npz")

@mnist_raw_ingred.capture
def get_raw():
	tf.keras.datasets.mnist.load_data()


if __name__ == '__main__':

    ex = Experiment("Get_Raw_MNIST", ingredients=[mnist_raw_ingred])

    @ex.main
    def main():
    	get_raw()

    ex.run_commandline()