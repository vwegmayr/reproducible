import sacred

class Ingredient(sacred.Ingredient):
	"""Wrapper around Sacred Ingredient

	This wrapper serves the purpose to make config entries,
	and commands directly available to inheriting ingredients, and
	avoids nesting of configs.

	Examples:

		ingred1 = sacred.Ingredient("ingred1")

		@ingred1.config
		def config1():
			param1 = ...

		ingred2 = sacred.Ingredient("ingred2", ingredients=[ingred1])
		
		@ingred2.capture
		def func(ingred1):
			use(ingred1["param1"])

		vs

		ingred1 = wrapper.Ingredient("ingred1")

		@ingred1.config
		def config1():
			param1 = ...

		ingred2 = wrapper.Ingredient("ingred2", ingredients=[ingred1])
		
		@ingred2.capture
		def func(param1):
			use(param1)

	Attributes:
		Same as sacred.Ingredient, except:

		inherit (bool): If true, ingredient does not nest config entries,
			else it behaves just like a sacred ingredient.
	"""
	def __init__(self, *args, ingredients=[], inherit=True, **kwargs):

		super(Ingredient, self).__init__(*args, ingredients=ingredients, **kwargs)

		if inherit:
			for ingred in ingredients:
				for config in ingred.configurations:
					self.configurations.append(config)

				for fn in ingred.captured_functions:
					self.captured_functions.append(fn)

				for hk in ingred.config_hooks:
					self.config_hooks.append(hk)

				for cmd, fn in ingred.commands.items():
					self.commands.update({cmd: fn})

				self.sources = self.sources.union(ingred.sources)

				self.dependencies = self.dependencies.union(ingred.dependencies)

			self.ingredients = []