import os

import tensorflow as tf
import numpy as np


def bytes_feature(value):
    """Convert value to TF bytes feature

    Used during serialization of features

    Args:
        value: instance of bytes, bytes list, or np.array 

    Returns:
        tf.train.Feature
    """
    if isinstance(value, (list, tuple)):
        if not all([isinstance(x, bytes) for x in value]):
            raise ValueError("bytes_feature expects list of bytes")

    elif isinstance(value, bytes):
        value = [value]

    elif isinstance(value, np.ndarray):
        value = [value.flatten().tostring()]

    else:
        raise ValueError("bytes_feature got bad value")

    return tf.train.Feature(bytes_list=tf.train.BytesList(value=value))


def int64_feature(value):
    """Convert value to TF int64 feature

    Used during serialization of features

    Args:
        value: instance of int, int list, or np.int 

    Returns:
        tf.train.Feature
    """
    if isinstance(value, np.ndarray):
        if "int" not in value.dtype.name:
            raise ValueError("int64_feature requires dtype int")

        value = [int(x) for x in value.flatten()]

    elif isinstance(value, (list, tuple)):
        if not all([isinstance(x, int) for x in value]):
            raise ValueError("int64_feature expects all elements as int")

    elif isinstance(value, int):
        value = [value]

    elif isinstance(value, (np.int8, np.int16, np.int32, np.int64,
                            np.uint8, np.uint16, np.uint32, np.uint64)):
        value = [int(value)]
    else:
        raise ValueError("int64_feature got bad value")

    return tf.train.Feature(int64_list=tf.train.Int64List(value=value))


def float32_feature(value):
    """Convert value to TF float32 feature

    Used during serialization of features

    Args:
        value: instance of float, float list, or np.flot 

    Returns:
        tf.train.Feature
    """
    if isinstance(value, np.ndarray):
        value = [float(x) for x in value.flatten()]

    elif isinstance(value, (list, tuple)):
        if not all([isinstance(x, float) for x in value]):
            raise ValueError("float32_feature expects all elements as float")

    elif isinstance(value, float):
        value = [value]

    elif isinstance(value, (np.float16, np.float32, np.float64)):
        value = [float(value)]

    else:
        raise ValueError("float32_feature got bad value")  

    return tf.train.Feature(float_list=tf.train.FloatList(value=value))


class PrintOnce(object):
    """Utility to print a message only once

    Args:
        message: Formattable string 
    """
    def __init__(self, message):
        super(PrintOnce, self).__init__()
        self.message = message
        self._printed = False

    def print(self, *args, **kwargs):
        if not self._printed:
            print(self.message.format(*args, **kwargs))
            self._printed = True


def mkdir_and_join(record_dir, record_pattern):
    """
    Args:
        record_dir (str): Path to record directory
        record_pattern (str): Record file pattern  

    Returns:
        (str): Full record file pattern, including directoy
    """
    if not os.path.exists(record_dir):
        os.mkdir(record_dir)

    return os.path.join(record_dir, record_pattern)

def to_int_size(split_to_size, n_samples):

    if all([isinstance(size, int) for size in split_to_size.values()]):
        return split_to_size
    else:
        return {key: int(val * n_samples) for key, val in split_to_size.items()}